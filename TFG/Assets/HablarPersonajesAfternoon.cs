﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.UI;
using Mono.Data.Sqlite; 
using System.Data; 
using System;

public class HablarPersonajesAfternoon : MonoBehaviour {

	public Flowchart fc;
	public Flowchart fcChicos;
	public Flowchart fcChicas;
	public GameObject miguel;
	private Transform auxiliarMiguel;

	public GameObject Carol;
	private Transform auxiliarCarol;

	//Evento chicos
	public GameObject Claudio;
	private Transform auxiliarClaudio;
	private GameObject Patricia;
	private GameObject David;
	private Transform auxiliarDavid;

	//Accion libro
	public GameObject Libro;

	//Preguntas:
	string pregunta7 = "¿He dicho mentiras sobre otro estudiante?";
	string pregunta13 = "Le he tirado algo a otro estudiante?";
	string pregunta33 = "Me han llamado nombres malos?";
	string pregunta58 = "Defendi a alguien a quien le quitaron cosas aproposito?";
	string pregunta30="Cuando alguien mas le quito aproposito los libros de las manos de otro estudiante, me rei?"; 
	string pregunta27="Me he reido de alguien cuando le estaban poniendo motes";
	string pregunta3 ="He dejado excluido a otro estudiante?";
	string pregunta39 ="La gente ha dicho mentiras sobre mi?";
	string pregunta76="Fingi no notar cuando alguien mas tropezo con otro estudiante aproposito?";


	float speed = 1; 

	public Text aux;
	int estado = 0;
	private bool Rotar = false;
	private bool RotarCarol = false;
	private int ControlFinal =0;

	//Base de Datos: 
	string conn;
	IDbConnection dbconn;
	IDbCommand dbcmd;
	IDataReader reader;
	string sqlQuery;

	//Variables auxiliares: 
	bool escribir1vez = true;
	private int idUser;


	// Use this for initialization
	void Start () {
		auxiliarMiguel = miguel.GetComponent<Transform>();
		auxiliarCarol = Carol.GetComponent<Transform>();
		auxiliarClaudio = Claudio.GetComponent<Transform>();
		Patricia = GameObject.Find ("Patricia");
		David = GameObject.Find ("David");
		auxiliarDavid = David.GetComponent<Transform> ();

		//Base de Datos: 
		/*conn = "URI=file:" + "C:/Users/ClaudioJose/Documents/TFG/Assets/PluginsBD/Bullying-TFG.db";
		Debug.Log ("ruta "+ conn.ToString());
		dbconn = (IDbConnection) new SqliteConnection(conn);
		dbconn.Open(); //Open connection to the database.
		dbcmd = dbconn.CreateCommand();

		sqlQuery = "SELECT * FROM Users";
		dbcmd.CommandText = sqlQuery;
		reader = dbcmd.ExecuteReader();

		while (reader.Read ()) 
		{
			int ID = reader.GetInt32 (0);
			string nameUSer = reader.GetString (1);

			Debug.Log ("ID= " + ID + "  NAME_USER =" + nameUSer);
			idUser = ID;
		}*/
	}

	// Update is called once per frame
	void Update () {
		//Hablar con el director.
		if(estado == 1){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				RotarObjeto ("DirectorAfternoon", 25);
				fc.ExecuteBlock ("Director");
			}
		}
		//Hablar con el Arthur.
		if(estado == 2){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcChicos.ExecuteBlock ("Arthur");
			}
		}
		//Hablar con la Profesora Amy.
		if(estado == 3){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcChicas.ExecuteBlock ("ProfesoraAmy");
			}
		}
		//Hablar con el profesor emilio.
		if(estado == 4){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcChicos.ExecuteBlock ("ProfesorEmilio");
			}
		}
		//Hablar con el Manu.
		if(estado == 5){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcChicos.ExecuteBlock ("Manu");
			}
		}
		//Hablar con el GuardaErick.
		if(estado == 6){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				RotarObjeto ("GuardiaErick", 25);
				fcChicos.ExecuteBlock ("GuardiaErick");
			}
		}
		//Hablar con Miguel.
		if(estado == 7){
			if(Input.GetKey(KeyCode.E)){
				Rotar = true;
				aux.text = "";
				estado = 0;
				RotarObjeto ("Miguel", 25);
				//auxiliarMiguel.Rotate (0,180,0);
				fcChicos.ExecuteBlock ("Miguel");
			}
		}
		//Hablar con Naby.
		if(estado == 8){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcChicas.ExecuteBlock ("Carol");
			}
		}
		//Hablar con Naby.
		if(estado == 9){
			if(Input.GetKey(KeyCode.E)){
				RotarCarol = true;
				aux.text = "";
				estado = 0;
				auxiliarCarol.Rotate (0,270,0);
				fcChicas.ExecuteBlock ("Carol");
			}
		}
		//Hablar con Naby.
		if(estado == 10){
			if(Input.GetKey(KeyCode.E)){
				RotarCarol = true;
				aux.text = "";
				estado = 0;
				auxiliarCarol.Rotate (0,180,0);
				fcChicas.ExecuteBlock ("Carol");
			}
		}
		//Hablar con Claudio-Prueba.
		if(estado == 11){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;

				fcChicos.ExecuteBlock ("ChicosAfternoon");
			}
		}
		if (estado==13)
		{
			if (Input.GetKey (KeyCode.E)) {
				aux.text = "";
				fcChicos.ExecuteBlock ("DiegoyLuis");
				estado = 0;
			}
		}
		if (estado==12)
		{
			fc.ExecuteBlock ("Libro");
			estado = 0;
		}
		if (ControlFinal == 7){
			GameObject flecha;
			flecha = GameObject.Find ("FlechaSalidaAfternoon");
			Animator anima = flecha.GetComponent<Animator>();
			anima.Play("MoverFlechaAfternoon");
			fc.ExecuteBlock ("Salida");

			/*if (escribir1vez ==true)
			{
				//Escritura base de datos: 
				bool respuesta7 = fc.GetVariable<BooleanVariable>("Pregunta7").Value;
				escribirBD (pregunta7,respuesta7, "Bully");
				bool respuesta13 = fc.GetVariable<BooleanVariable>("Pregunta13").Value;
				escribirBD (pregunta13,respuesta13, "Bully");
				bool respuesta33 = fcChicos.GetVariable<BooleanVariable> ("Pregunta33").Value;
				escribirBD (pregunta33,respuesta33, "Victim");
				bool respuesta58 = fcChicos.GetVariable<BooleanVariable> ("Pregunta58").Value;
				escribirBD (pregunta58,respuesta58, "Defender");
				bool respuesta3027 = fcChicas.GetVariable<BooleanVariable> ("Pregunta3027").Value;
				escribirBD (pregunta30,respuesta3027, "Asistant");
				escribirBD (pregunta27,respuesta3027, "Asistant");
				bool respuesta3 = fcChicos.GetVariable<BooleanVariable> ("Pregunta3").Value;
				escribirBD (pregunta3,respuesta3, "Bully");
				bool respuesta76 = fcChicos.GetVariable<BooleanVariable> ("Pregunta76").Value;
				escribirBD (pregunta76,respuesta76, "Outsider");
				bool respuesta39 = fcChicos.GetVariable<BooleanVariable> ("Pregunta39").Value;
				escribirBD (pregunta39,respuesta39, "Victim");

				escribir1vez = false;
				CerrarBD ();

			}*/

		}
		Debug.Log( "ControlFinal= "+ControlFinal);
	}

	private void OnTriggerEnter(Collider otro)
	{
		switch (otro.name) {
			case "DirectorAfternoon": 
				aux.text = "Pulsa E para Hablar con el";
				estado = 1;
				Block director = fc.FindBlock ("Director");
				int number = director.GetExecutionCount();
				if(number == 1){
					ControlFinal--;
				}
				break;
			case "Arthur": 
				aux.text = "Pulsa E para Hablar con el";
				estado = 2;
				break;
			case "ProfesoraAmy": 
				aux.text = "Pulsa E para Hablar con ella";
				estado = 3;
				break;
			case "ProfesorEmilio": 
				aux.text = "Pulsa E para Hablar con el";
				estado = 4;
				break;
			case "Manu": 
				aux.text = "Pulsa E para Hablar con el";
				estado = 5;
				break;
			case "GuardiaErick": 
				aux.text = "Pulsa E para Hablar con el";
				estado = 6;
				Block guardia = fcChicos.FindBlock ("GuardiaErick");
				int numberGuardia = guardia.GetExecutionCount();
				if(numberGuardia == 1){
					ControlFinal--;
				}
				break;
			case "Miguel": 
				aux.text = "Pulsa E para Hablar con el";
				estado = 7;
				Block miguel = fcChicos.FindBlock ("Miguel");
				int numberMiguel = miguel.GetExecutionCount();
				if(numberMiguel == 1){
					ControlFinal--;
				}
				break;
			case "ColliderAux": 
				aux.text = "Pulsa E para Hablar con ella";
				estado = 8;
				Block Carol = fcChicas.FindBlock ("Carol");
				int numberCarol = Carol.GetExecutionCount();
				if(numberCarol == 1){
					ControlFinal--;
				}
				break;
			case "ColliderAux1": 
				aux.text = "Pulsa E para Hablar con ella";
				estado = 9;
				Block CarolAux = fcChicas.FindBlock ("Carol");
				int numberCarolAux = CarolAux.GetExecutionCount();
				if(numberCarolAux == 1){
					ControlFinal--;
				}
				break;
			case "ColliderAux2": 
				aux.text = "Pulsa E para Hablar con ella";
				estado = 10;
				Block CarolAux1 = fcChicas.FindBlock ("Carol");
				int numberCarolAux1 = CarolAux1.GetExecutionCount();
				if(numberCarolAux1 == 1){
					ControlFinal--;
				}
				break;
			case "GirarChicos": 
				aux.text = "Pulsa E para Hablar con ellos";
				estado = 11;
				Block chicos = fcChicos.FindBlock ("ChicosAfternoon");
				int numberChicos = chicos.GetExecutionCount();
				if(numberChicos == 1){
					ControlFinal--;
				}
				break;
			case "Libro":
				estado = 12;
				ControlFinal++;
				break;
			case "ColliderChicos": 
				aux.text = "Pulsa E para Hablar con ellos";
				estado = 13;
				Block DiegoyLuis = fcChicos.FindBlock ("DiegoyLuis");
				int numberChicosDiego = DiegoyLuis.GetExecutionCount();
				if(numberChicosDiego == 1){
					ControlFinal--;
				}
				break;
			
		}
	}

	private void OnTriggerExit(Collider otro)
	{
		switch (otro.name) {
			case "DirectorAfternoon": 
				aux.text = "";
				estado = 0;
				Block director = fc.FindBlock ("Director");
				int number = director.GetExecutionCount();
				if(number == 1){
					ControlFinal++;
				}
				break;
			case "Arthur": 
				aux.text = "";
				estado = 0;
				break;
			case "ProfesoraAmy": 
				aux.text = "";
				estado = 0;
				break;
			case "ProfesorEmilio": 
				aux.text = "";
				estado = 0;
				break;
			case "Manu": 
				aux.text = "";
				estado = 0;
				break;
			case "GuardiaErick": 
				aux.text = "";
				estado = 0;
				Block guardia = fcChicos.FindBlock ("GuardiaErick");
				int numberGuardia = guardia.GetExecutionCount();
				if(numberGuardia == 1){
					ControlFinal++;
				}
				break;
			case "Miguel": 
				aux.text = "";
				estado = 0;
				Block miguel = fcChicos.FindBlock ("Miguel");
				int numberMiguel = miguel.GetExecutionCount();
				if(numberMiguel == 1){
					ControlFinal++;
				}
				break;
			case "ColliderAux": 
				aux.text = "";
				estado = 0;
				Block Carol = fcChicas.FindBlock ("Carol");
				int numberCarol = Carol.GetExecutionCount();
				if(numberCarol == 1){
					ControlFinal++;
				}
				break;

			case "ColliderAux1": 
				aux.text = "";
				if (RotarCarol == true) {
					auxiliarCarol.Rotate (0, 90, 0);
					}
				RotarCarol = false;
				estado = 0;
				Block CarolAux = fcChicas.FindBlock ("Carol");
				int numberCarolAux = CarolAux.GetExecutionCount();
				if(numberCarolAux == 1){
					ControlFinal++;
				}
				break;
			case "ColliderAux2": 
				aux.text = "";
				if (RotarCarol == true) {
					auxiliarCarol.Rotate (0, 180, 0);
					}
				RotarCarol = false;
				estado = 0;
				Block CarolAux1 = fcChicas.FindBlock ("Carol");
				int numberCarolAux1 = CarolAux1.GetExecutionCount();
				if(numberCarolAux1 == 1){
					ControlFinal++;
				}
				break;
			case "GirarChicos": 
				aux.text = "";
				estado = 0;
				Block chicos = fcChicos.FindBlock ("ChicosAfternoon");
				int numberChicos = chicos.GetExecutionCount();
				if(numberChicos == 1){
					ControlFinal++;
				}
				break;
			case "ColliderChicos": 
				aux.text = "";
				estado = 0;
				Block chicosDiegoyLuis = fcChicos.FindBlock ("DiegoyLuis");
				int numberChicosDiegoyLuis = chicosDiegoyLuis.GetExecutionCount();
				if(numberChicosDiegoyLuis == 1){
					ControlFinal++;
				}
				break;

		}
	}

	void RotarObjeto( string aux, int grados)
	{
		GameObject ObjectAux;
		ObjectAux = GameObject.Find (aux);
		ObjectAux.GetComponent<Transform> ().LookAt (transform);
		ObjectAux.GetComponent<Transform> ().Rotate (grados, 0, 0);

	}
	public void RotarChicos()
	{
		auxiliarClaudio.LookAt(transform);
		auxiliarClaudio.Rotate (15,0,0);
		auxiliarDavid.LookAt (transform);
		auxiliarDavid.Rotate (15,0,0);
		Patricia.GetComponent<Transform>().LookAt (transform);
		Patricia.GetComponent<Transform>().Rotate (15,0,0);
	}

	void CerrarBD ()
	{
		reader.Close ();
		reader = null;
		dbcmd.Dispose ();
		dbcmd = null;
		dbconn.Close ();
		dbconn = null;
	}

	void escribirBD(string pregunta, bool respuesta, string tipoQuestion)
	{
		string respuestaAux="";
		respuestaAux = respuesta.ToString ();
		conn = "URI=file:" + Application.dataPath + "/PluginsBD/Bullying-TFG.db";
		dbconn = (IDbConnection) new SqliteConnection(conn);
		dbconn.Open(); //Open connection to the database.
		dbcmd = dbconn.CreateCommand();
		sqlQuery = "INSERT INTO BullyingQuestion (Question, Answer,Type_Question, ID_USER) VALUES ('"+pregunta+"', '"+respuestaAux +"', '"+tipoQuestion +"', '"+ idUser + "')";
		dbcmd.CommandText = sqlQuery;
		dbcmd.ExecuteNonQuery();
	}

	public void RotarDiego(){
		GameObject Diego;
		Diego = GameObject.Find ("DiegoEscaleras");
		Diego.GetComponent<Transform>().LookAt (transform);
		Diego.GetComponent<Transform>().Rotate (55,180,0);
	}

}

