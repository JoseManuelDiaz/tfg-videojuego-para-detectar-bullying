﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.UI;
using Mono.Data.Sqlite; 
using System.Data; 
using System;

public class HablarPersonajesNigth : MonoBehaviour {
	public Flowchart fc;
	public Flowchart fcChicos;
	public Text aux;
	int estado = 0;
	bool Chicosnigth = true;
	int ControlFinal =0;

	//Preguntas:
	string pregunta62 = "¿Ayude a alguien que fue intencionalmente engañado?";
	string pregunta79 = "Ignore cuando alguien mas engaño a otro estudiante";
	string pregunta26 = "Me he burlado de alguien, cuando lo empujaron, golpearon o abofetearon?";
	string pregunta64 = "Defendi a alguien que creai que estaba siendo engañado aproposito?";
	string pregunta29 = "cuando alguien más hizo tropezar a otro estudiante a propósito, me reí?";

	//Base de Datos: 
	string conn;
	IDbConnection dbconn;
	IDbCommand dbcmd;
	IDataReader reader;
	string sqlQuery;

	//Variables auxiliares: 
	bool escribir1vez = true;
	private int idUser;

	// Use this for initialization
	void Start () 
	{
		//Base de Datos: 
		/*conn = "URI=file:" + "C:/Users/ClaudioJose/Documents/TFG/Assets/PluginsBD/Bullying-TFG.db";
		Debug.Log ("ruta "+ conn.ToString());
		dbconn = (IDbConnection) new SqliteConnection(conn);
		dbconn.Open(); //Open connection to the database.
		dbcmd = dbconn.CreateCommand();

		sqlQuery = "SELECT * FROM Users";
		dbcmd.CommandText = sqlQuery;
		reader = dbcmd.ExecuteReader();

		while (reader.Read ()) 
		{
			int ID = reader.GetInt32 (0);
			string nameUSer = reader.GetString (1);

			Debug.Log ("ID= " + ID + "  NAME_USER =" + nameUSer);
			idUser = ID;
		}*/
	}
	
	// Update is called once per frame
	void Update () {
		//Hablar con el policia.
		if(estado == 1){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				RotarObjeto ("Policia3",25);
				fc.ExecuteBlock ("Policia3");
			}
		}
		//Hablar con el policia.
		if(estado == 2){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				RotarObjeto ("Policia1",25);
				fc.ExecuteBlock ("Policia1");
			}
		}
		//Hablar con el policia.
		if(estado == 3){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				RotarObjeto ("Policia2",25);
				fc.ExecuteBlock ("Policia2");
			}
		}
		//Hablar con los chicos por la noche.
		if(estado == 4){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcChicos.ExecuteBlock ("Chicos");
			}
		}

		if (ControlFinal == 2){
			GameObject flecha;
			flecha = GameObject.Find ("FlechaSalida");
			Animator anima = flecha.GetComponent<Animator>();
			anima.Play("MoverFlechaNight");
			fc.ExecuteBlock ("Final");

			/*if (escribir1vez ==true)
			{
				//Escritura base de datos: 
				bool respuesta6279 = fc.GetVariable<BooleanVariable>("Pregunta6279").Value;
				escribirBD (pregunta62,respuesta6279, "Defender");
				escribirBD (pregunta79,respuesta6279, "Outsider");
				bool respuesta26 = fcChicos.GetVariable<BooleanVariable>("Pregunta26").Value;
				escribirBD (pregunta26,respuesta26, "Asistant");
				bool respuesta64 = fcChicos.GetVariable<BooleanVariable> ("Pregunta64").Value;
				escribirBD (pregunta64,respuesta64, "Defender");
				bool respuesta29 = fcChicos.GetVariable<BooleanVariable>("Pregunta29").Value;
				escribirBD (pregunta29,respuesta29, "Asistant");

				escribir1vez = false;
				CerrarBD ();

			}*/
		}
		Debug.Log( "ControlFinal= "+ControlFinal);

		
	}

	private void OnTriggerEnter(Collider otro)
	{
		switch (otro.name) {
			case "Policia3": 
				aux.text = "Pulsa E para Hablar con el";
				estado = 1;

				break;

			case "Policia1": 
				aux.text = "Pulsa E para Hablar con el";
				estado = 2;
				break;
		
			case "Policia2": 
				aux.text = "Pulsa E para Hablar con el";
				estado = 3;
				Block policia = fc.FindBlock ("Policia2");
				int number = policia.GetExecutionCount();
				if(number == 1){
					ControlFinal--;
				}
				break;
			case "ChicosNigth": 
				if (Chicosnigth == true){
					aux.text = "Pulsa E para Hablar con ellos";
					estado = 4;
					Chicosnigth = false;
				}
				Block chicos = fcChicos.FindBlock ("Chicos");
				int numberChicos = chicos.GetExecutionCount();
				if(numberChicos == 1){
					ControlFinal--;
				}
				break;
		}
	}

	private void OnTriggerExit(Collider otro)
	{
		switch (otro.name) {
			case "Policia3": 
				aux.text = "";
				estado = 0;
				break;
			case "Policia1": 
				aux.text = "";
				estado = 0;
				break;
			case "Policia2": 
				aux.text = "";
				estado = 0;
				Block policia = fc.FindBlock ("Policia2");
				int number = policia.GetExecutionCount();
				if(number == 1){
					ControlFinal++;
				}
				break;
			case "ChicosNigth": 
				aux.text = "";
				estado = 0;
				Block chicos = fcChicos.FindBlock ("Chicos");
				int numberChicos = chicos.GetExecutionCount();
				if(numberChicos == 1){
					ControlFinal++;
				}
				break;
		}
	}

	void RotarObjeto( string aux, int grados)
	{
		GameObject ObjectAux;
		ObjectAux = GameObject.Find (aux);
		ObjectAux.GetComponent<Transform> ().LookAt (transform);
		ObjectAux.GetComponent<Transform> ().Rotate (grados, 0, 0);

	}

	public void RotarClaudio()
	{
		GameObject claudio;
		claudio = GameObject.Find ("Claudio");
		claudio.GetComponent<Transform>().Rotate (75,90,0);
	}

	void CerrarBD ()
	{
		reader.Close ();
		reader = null;
		dbcmd.Dispose ();
		dbcmd = null;
		dbconn.Close ();
		dbconn = null;
	}

	void escribirBD(string pregunta, bool respuesta, string tipoQuestion)
	{
		string respuestaAux="";
		respuestaAux = respuesta.ToString ();
		conn = "URI=file:" + Application.dataPath + "/PluginsBD/Bullying-TFG.db";
		dbconn = (IDbConnection) new SqliteConnection(conn);
		dbconn.Open(); //Open connection to the database.
		dbcmd = dbconn.CreateCommand();
		sqlQuery = "INSERT INTO BullyingQuestion (Question, Answer,Type_Question, ID_USER) VALUES ('"+pregunta+"', '"+respuestaAux +"', '"+tipoQuestion +"', '"+ idUser + "')";
		dbcmd.CommandText = sqlQuery;
		dbcmd.ExecuteNonQuery();
	}
}
