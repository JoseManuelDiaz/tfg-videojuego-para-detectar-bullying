﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using UnityEngine.UI;
using Mono.Data.Sqlite; 
using System.Data; 
using System;

public class Hablar : MonoBehaviour {

	// Use this for initialization
	public Flowchart fc;
	public Flowchart fcBoys;
	public Flowchart fcGirls;
	public Text aux;

	private bool empezar= true;

	private int estado = 0;

	private int ControlFinal =0;
	private int idUser;

	//Preguntas:
	string pregunta36 = "¿Alguna vez haz sido ignorado?";
	string pregunta15 = "He dicho cosas malas sobre otro estudiante?";
	string pregunta34 = "se han he burlado de mi?";
	string pregunta35 = "a propósito, me he quedado fuera de algo?";
	string pregunta59="defendí a alguien a quien se llamaba nombres malos?";
	string pregunta63="cuando vi que alguien sufría daños físicos, le dije a un adulto?";
	string pregunta77="Lo ignoré cuando alguien más pellizcó o golpeó a otro estudiante?";
	string pregunta78="Lo ignoré cuando algunos arrojaron algo a otro estudiante?";
	string pregunta25 = "Cuando alguien se topó con otra persona, me uní?";
	string pregunta27 = "Me he burlado de alguien a quien se llamaba nombres malos?";

	//Base de Datos: 
	string conn;
	IDbConnection dbconn;
	IDbCommand dbcmd;
	IDataReader reader;
	string sqlQuery;

	//Variables auxiliares: 
	bool escribir1vez = true;

	void Start () {
		/*conn = "URI=file:" + "C:/Users/ClaudioJose/Documents/TFG/Assets/PluginsBD/Bullying-TFG.db";
		Debug.Log ("ruta "+ conn.ToString());
		dbconn = (IDbConnection) new SqliteConnection(conn);
		dbconn.Open(); //Open connection to the database.
		dbcmd = dbconn.CreateCommand();

		sqlQuery = "SELECT * FROM Users";
		dbcmd.CommandText = sqlQuery;
		reader = dbcmd.ExecuteReader();

		while (reader.Read())
		{
			int ID = reader.GetInt32(0);
			string nameUSer = reader.GetString(1);

			Debug.Log( "ID= "+ID+"  NAME_USER ="+nameUSer);
			idUser = ID;
		}*/
	}
	
	// Update is called once per frame
	void Update () {
		//Hablar con el director.
		if(estado == 1){
			if(Input.GetKey(KeyCode.E)){
				GameObject Director;
				aux.text = "";
				if (empezar) {
					estado = 0;

					Director = GameObject.Find ("DirectorMorning");
					Director.GetComponent<Transform>().LookAt (transform);
					Director.GetComponent<Transform>().Rotate (30,0,0);
					fc.ExecuteBlock ("Director");
					empezar = false;
				} else
					Director = GameObject.Find ("DirectorMorning");
					Director.GetComponent<Transform>().LookAt (transform);
					Director.GetComponent<Transform>().Rotate (30,0,0);
					fc.ExecuteBlock ("Director");
			}

		}
		//Hablar con JaviEntrance
		if(estado == 2){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcBoys.ExecuteBlock ("JaviChat");
			}
		}
		//Hablar con JuliEntrance
		if(estado == 3){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcBoys.ExecuteBlock ("JuliChat");
			}
		}
		//Hablar con JuliaEntrance
		if(estado == 4){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcGirls.ExecuteBlock ("JuliaChat");
			}
		}
		//Hablar con AngelaEntrance
		if(estado == 5){
			if(Input.GetKey(KeyCode.E)){
				GameObject Angela;
				aux.text = "";
				Angela = GameObject.Find ("AngelaPasillo");
				Angela.GetComponent<Transform>().LookAt (transform);
				Angela.GetComponent<Transform>().Rotate (30,0,0);
				estado = 0;
				fcGirls.ExecuteBlock ("AngelaChat");
			}
		}
		//Hablar con ClaudiaEntrance
		if(estado == 6){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcGirls.ExecuteBlock ("ClaudiaChat");
			}
		}
		//Hablar con RamonEntrance
		if(estado == 7){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcBoys.ExecuteBlock ("RamonChat");
			}
		}
		//Hablar con SergioEscaleras
		if(estado == 8){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcBoys.ExecuteBlock ("SergioChat");
			}
		}
		//Hablar con EmilioEscaleras
		if(estado == 9){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcBoys.ExecuteBlock ("EmilioChat");
			}
		}
		//Hablar con EmilyEscaleras
		if(estado == 10){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				RotarObjeto ( "EmilyEscaleras");
				estado = 0;
				fcGirls.ExecuteBlock ("EmilyChat");
			}
		}
		//Hablar con IvanEscaleras y DianaEscaleras.
		if(estado == 11){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcBoys.ExecuteBlock ("IvanYDianaChat");
			}
		}
		//Hablar con EvaClassRoom y ThomasClassRoom.
		if(estado == 12){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcGirls.ExecuteBlock ("EvaYThomasChat");
			}
		}
		//Hablar con alexClassRoom.
		if(estado == 13){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				estado = 0;
				fcBoys.ExecuteBlock ("AlexChat");
			}
		}
		//Hablar con CarolClassRoom.
		if(estado == 14){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				GameObject Carol;
				Carol = GameObject.Find ("CarolClassRoom");
				Carol.GetComponent<Transform>().LookAt (transform);
				Carol.GetComponent<Transform>().Rotate (30,0,0);
				estado = 0;
				fcGirls.ExecuteBlock ("CarolChat");
			}
		}
		//Hablar con AliceClassRoom.
		if(estado == 15){
			if(Input.GetKey(KeyCode.E)){
				GameObject Alice;
				aux.text = "";
				Alice = GameObject.Find ("ProfesoraAliceClassRoom");
				Alice.GetComponent<Transform>().LookAt (transform);
				Alice.GetComponent<Transform>().Rotate (30,0,0);
				estado = 0;
				fcGirls.ExecuteBlock ("AliceChat");
			}
		}
		//Hablar con ProfesoraIreneClassRoom y ProfesorFran.
		if(estado == 16){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				RotarObjeto ("ProfesoraIrene");
				RotarObjeto ("ProfesorFran");
				estado = 0;
				fcGirls.ExecuteBlock ("IreneProfesoraChat");
			}
		}
		//Evento principal.
		if(estado == 17){
			estado = 0;
			fcBoys.ExecuteBlock ("ChicosChat");
		}

		//Hablar con MayaClassRoom.
		if(estado == 18){
			if(Input.GetKey(KeyCode.E)){
				aux.text = "";
				RotarObjeto ("MayaClassRoom");
				estado = 0;
				fcGirls.ExecuteBlock ("MayaChat");
			}
		}

		if (ControlFinal == 6){
			GameObject flecha;
			flecha = GameObject.Find ("FlechaSalida");
			Animator anima = flecha.GetComponent<Animator>();
			anima.Play("MoverFlecha");
			fc.ExecuteBlock ("Salida");

			/*if (escribir1vez ==true)
			{
				//Escritura base de datos: 
				bool respuesta36 = fc.GetVariable<BooleanVariable>("Pregunta36").Value;
				escribirBD (pregunta36,respuesta36,"Victim");
				bool respuesta15 = fcGirls.GetVariable<BooleanVariable>("Pregunta15").Value;
				escribirBD (pregunta15,respuesta15, "Bully");
				bool respuesta34 = fcGirls.GetVariable<BooleanVariable> ("Pregunta34").Value;
				escribirBD (pregunta34,respuesta34, "Victim");
				bool respuesta35 = fcGirls.GetVariable<BooleanVariable> ("Pregunta35").Value;
				escribirBD (pregunta35,respuesta35,"Victim");
				bool respuesta59 = fcGirls.GetVariable<BooleanVariable> ("Pregunta59").Value;
				escribirBD (pregunta59,respuesta59, "Defender");
				bool respuesta63 = fcBoys.GetVariable<BooleanVariable> ("Pregunta637778").Value;
				bool respuesta25 = fcBoys.GetVariable<BooleanVariable> ("Pregunta25").Value;
				bool respuesta27 = fcGirls.GetVariable<BooleanVariable> ("Pregunta27").Value;

				escribirBD (pregunta63,respuesta63, "Defender");
				escribirBD (pregunta77,respuesta63, "Outsider");
				escribirBD (pregunta78,respuesta63, "Outsider");
				escribirBD (pregunta25, respuesta25,"Assistant");
				escribirBD (pregunta27, respuesta27,"Assistant");


				escribir1vez = false;
				CerrarBD ();
			}*/
		}
		Debug.Log( "ControlFinal= "+ControlFinal);
	}

	void RotarObjeto( string aux)
	{
		GameObject Emily;
		Emily = GameObject.Find (aux);
		Emily.GetComponent<Transform> ().LookAt (transform);
		Emily.GetComponent<Transform> ().Rotate (30, 0, 0);

	}

	private void OnTriggerEnter(Collider otro)
	{

		switch (otro.name) {

		case "DirectorMorning": 
			aux.text = "Pulsa E para Hablar con el";
			estado = 1;
			Block director = fc.FindBlock ("Director");
			int number = director.GetExecutionCount();
			if(number == 1){
				ControlFinal--;
			}
		break;

		case "JaviEntrance":
			aux.text = "Pulsa E para Hablar con el";
			estado = 2;
		break;

		case "JuliEntrance":
			aux.text = "Pulsa E para Hablar con el";
			estado = 3;
		break;

		case "JuliaEntrance":
			aux.text = "Pulsa E para Hablar con ella";
			estado = 4;
			break;
		
		case "AngelaPasillo":
			aux.text = "Pulsa E para Hablar con ella";
			estado = 5;
			Block Angela = fcGirls.FindBlock ("AngelaChat");
			int numberAngela = Angela.GetExecutionCount();
			if(numberAngela == 1){
				ControlFinal--;
				//En este if meter la escritura a la base de datos.
			}
			break;
		case "ClaudiaPasillo":
			aux.text = "Pulsa E para Hablar con ella";
			estado = 6;
			break;
		case "RamónPasillo":
			aux.text = "Pulsa E para Hablar con el";
			estado = 7;
			break;
		case "SergioEscaleras":
			aux.text = "Pulsa E para Hablar con el";
			estado = 8;
			break;
		case "EmilioEscaleras":
			aux.text = "Pulsa E para Hablar con el";
			estado = 9;
			break;
		case "EmilyEscaleras":
			aux.text = "Pulsa E para Hablar con ella";
			estado = 10;
			break;
		case "IvanEscaleras":
			aux.text = "Pulsa E para Hablar con ellos";
			estado = 11;
			Block ivan = fcBoys.FindBlock ("IvanYDianaChat");
			int numberivan = ivan.GetExecutionCount();
			if(numberivan == 1){
				ControlFinal--;
				//En este if meter la escritura a la base de datos.
			}
			break;
		case "EvaClassRoom":
			aux.text = "Pulsa E para Hablar con ellos";
			estado = 12;
			Block eva = fcGirls.FindBlock ("EvaYThomasChat");
			int numbereva = eva.GetExecutionCount();
			if(numbereva == 1){
				ControlFinal--;
			}
			break;
		case "AlexClassRoom":
			aux.text = "Pulsa E para Hablar con el";
			estado = 13;
			break;
		case "CarolClassRoom":
			aux.text = "Pulsa E para Hablar con ella";
			estado = 14;
			Block Carol = fcGirls.FindBlock ("CarolChat");
			int numberCarol = Carol.GetExecutionCount();
			if(numberCarol == 1){
				ControlFinal--;
			}
			break;
		case "ProfesoraAliceClassRoom":
			aux.text = "Pulsa E para Hablar con la profesora";
			estado = 15;
			break;
		case "ProfesoraIrene":
			aux.text = "Pulsa E para Hablar con ellos";
			estado = 16;
			break;
		case "EventoPrincipal":
			estado = 17;
			break;
		case "MayaClassRoom":
			aux.text = "Pulsa E para Hablar con ella";
			estado = 18;
			Block Maya = fcGirls.FindBlock ("MayaChat");
			int numberaux = Maya.GetExecutionCount();
			if(numberaux == 1){
				ControlFinal--;
			}
			break;
		}
	}

	private void OnTriggerExit(Collider otro)
	{
		switch (otro.name) {

		case "DirectorMorning": 
			aux.text = "";
			estado = 0;
			Block director = fc.FindBlock ("Director");
			int number = director.GetExecutionCount();
			if(number == 1){
				ControlFinal++;
				//En este if meter la escritura a la base de datos.
			}
			break;
		case "JaviEntrance":
			aux.text = "";
			estado = 0;
			break;

		case "JuliEntrance":
			aux.text = "";
			estado = 0;
			break;

		case "JuliaEntrance":
			aux.text = "";
			estado = 0;
			break;

		case "AngelaPasillo":
			aux.text = "";
			estado = 0;
			Block Angela = fcGirls.FindBlock ("AngelaChat");
			int numberAngela = Angela.GetExecutionCount();
			if(numberAngela == 1){
				ControlFinal++;
				//En este if meter la escritura a la base de datos.
			}
			break;
		case "ClaudiaPasillo":
			aux.text = "";
			estado = 0;
			break;
		case "RamónPasillo":
			aux.text = "";
			estado = 0;
			break;
		case "SergioEscaleras":
			aux.text = "";
			estado = 0;
			break;
		case "EmilioEscaleras":
			aux.text = "";
			estado = 0;
			break;
		case "EmilyEscaleras":
			aux.text = "";
			estado = 0;
			break;
		case "IvanEscaleras":
			aux.text = "";
			estado = 0;
			Block ivan = fcBoys.FindBlock ("IvanYDianaChat");
			int numberivan = ivan.GetExecutionCount();
			if(numberivan == 1){
				ControlFinal++;
				//En este if meter la escritura a la base de datos.
			}
			break;
		case "EvaClassRoom":
			aux.text = "";
			estado = 0;
			Block eva = fcGirls.FindBlock ("EvaYThomasChat");
			int numbereva = eva.GetExecutionCount();
			if(numbereva == 1){
				ControlFinal++;
				//En este if meter la escritura a la base de datos.
			}
			break;
		case "AlexClassRoom":
			aux.text = "";
			estado = 0;
			break;
		case "CarolClassRoom":
			aux.text = "";
			estado = 0;
			Block Carol = fcGirls.FindBlock ("CarolChat");
			int numberCarol = Carol.GetExecutionCount();
			if(numberCarol == 1){
				ControlFinal++;
				//En este if meter la escritura a la base de datos.
			}
			break;
		case "ProfesoraAliceClassRoom":
			aux.text = "";
			estado = 0;
			break;
		case "ProfesoraIrene":
			aux.text = "";
			estado = 0;
			break;
		case "MayaClassRoom":
			aux.text = "";
			estado = 0;
			Block Maya = fcGirls.FindBlock ("MayaChat");
			int numberaux = Maya.GetExecutionCount();
			if(numberaux == 1){
				ControlFinal++;
				//En este if meter la escritura a la base de datos.
			}
			break;
		}


	}

	public void RotarDiana(){
		GameObject Diana;
		Diana = GameObject.Find ("DianaEscaleras");
		Diana.GetComponent<Transform>().LookAt (transform);
		Diana.GetComponent<Transform>().Rotate (55,180,0);
	}

	void CerrarBD ()
	{
		reader.Close ();
		reader = null;
		dbcmd.Dispose ();
		dbcmd = null;
		dbconn.Close ();
		dbconn = null;
	}

	void escribirBD(string pregunta, bool respuesta, string tipoQuestion)
	{
		string respuestaAux="";
		respuestaAux = respuesta.ToString ();
		conn = "URI=file:" + Application.dataPath + "/PluginsBD/Bullying-TFG.db";
		dbconn = (IDbConnection) new SqliteConnection(conn);
		dbconn.Open(); //Open connection to the database.
		dbcmd = dbconn.CreateCommand();
		sqlQuery = "INSERT INTO BullyingQuestion (Question, Answer,Type_Question, ID_USER) VALUES ('"+pregunta+"', '"+respuestaAux +"', '"+tipoQuestion +"', '"+ idUser + "')";
		dbcmd.CommandText = sqlQuery;
		dbcmd.ExecuteNonQuery();
	}
}
