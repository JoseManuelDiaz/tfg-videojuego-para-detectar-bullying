﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mono.Data.Sqlite; 
using System.Data; 
using System;

public class Puertas : MonoBehaviour {
	public Text aux;
	public Text Escenas;
	// Use this for initialization
	//Estas variables son para controlar las puertas del escenario de la mañana.//
	private bool estado = true;
	private bool estado1 = true;
	bool puertaAbierta= false;
	bool puertaAbierta1= false;
	private bool estado2 = true;
	bool puertaAbierta2= false;
	private bool estado3 = true;
	bool puertaAbierta3= false;

	//Estas son variables para controlar las puertas del escenario de la tarde.//
	private bool estado4 = true;
	bool puertaAbierta4= false;
	private bool estado5 = true;
	bool puertaAbierta5 = false;
	private bool estado6 = true;
	bool puertaAbierta6 = false;
	private bool estado7 = true;
	bool puertaAbierta7 = false;

	//Estas son variables para controlar las puertas del escenario de la noche.//
	private bool estado8 = true;
	bool puertaAbierta8 = false;
	private bool estado9 = true;
	bool puertaAbierta9 = false;
	private bool estado10 = true;
	bool puertaAbierta10 = false;
	private bool estado11 = true;
	bool puertaAbierta11 = false;
	private bool estado12 = true;
	private bool estado13 = true;
	private bool estado14 = true;



	void Start () {

	}


	// Update is called once per frame
	void Update () {
		if (estado == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("crm_doorR_b");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("AbrirCerrarPuerta");
				puertaAbierta = true;
				//estado = true;
			}
		}

		if (estado1 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("crm_doorL_f");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta2");
				puertaAbierta1 = true;
				//estado = true;
			}
		}
		if (estado2 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("crm_doorL_b2");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta3Abierta");
				puertaAbierta2 = true;
				//estado = true;
			}
		}

		if (estado3 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("crm_doorL_f2");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta4Abierta");
				puertaAbierta3 = true;
				//estado = true;
			}
		}

		//Estos if son para controlar las puertas de por la tarde.//
		if (estado4 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("HW_doorR_f_eve");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta1EveningAbierta");
				puertaAbierta4 = true;
				//estado = true;
			}
		}

		if (estado5 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("HW_doorR_b_eve");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta2EveningAbierta");
				puertaAbierta5 = true;
				//estado = true;
			}
		}

		if (estado6 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("HW_doorL_b_eve");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta3EveningAbierta");
				puertaAbierta6 = true;
				//estado = true;
			}
		}

		if (estado7 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("crm_doorL_f_eve");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta4EveningAbierta");
				puertaAbierta7 = true;
				//estado = true;
			}
		}

		//Estos if son para controlar las puertas de por la noche://
		/*if (estado8 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("HW_doorR_f_nig");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta1NightCerrada");
				puertaAbierta8 = true;
				//estado = true;
			}
		}*/

		if (estado9 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("HW_doorL_b_nig");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta2NightAbierta");
				puertaAbierta9 = true;
				//estado = true;
			}
		}

		if (estado10 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("HW_doorL_b_nig(2)");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta3NightAbierta");
				puertaAbierta10 = true;
				//estado = true;
			}
		}

		if (estado11 == false){
			if (Input.GetKey (KeyCode.E)) {
				GameObject x = GameObject.Find ("HW_doorR_f_nig(2)");
				Animator anima = x.GetComponent<Animator>();
				anima.Play("Puerta4NightAbierta");
				puertaAbierta11 = true;
				//estado = true;
			}
		}

		if (estado12 == false){
			if (Input.GetKey (KeyCode.E)) {
				Application.LoadLevel (2);
			}
		}

		if (estado13 == false){
			if (Input.GetKey (KeyCode.E)) {
				Application.LoadLevel (3);
			}
		}
		if (estado14 == false){
			if (Input.GetKey (KeyCode.E)) {
				Application.LoadLevel (4);
			}
		}

	}

	private void OnTriggerEnter(Collider otro)
	{
		//Estos if son para controlar las puertas de por la mañana.//
		if (otro.name == "crm_doorR_b"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado = false;
		}

		if (otro.name == "crm_doorL_f"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado1 = false;
		}
		if (otro.name == "crm_doorL_b2"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado2 = false;
		}
			
		if (otro.name == "crm_doorL_f2"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado3 = false;
		}
		//Estos if son para controlar las puertas de por la tarde.//
		if (otro.name == "HW_doorR_f_eve"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado4 = false;
		}

		if (otro.name == "HW_doorR_b_eve"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado5 = false;
		}

		if (otro.name == "HW_doorL_b_eve"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado6 = false;
		}
		if (otro.name == "crm_doorL_f_eve"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado7 = false;
		}
		//Estos if son para controlar las puertas de por la noche://
		/*if (otro.name == "HW_doorR_f_nig"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado8 = false;
		}*/
		if (otro.name == "HW_doorL_b_nig"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado9 = false;
		}
		if (otro.name == "HW_doorL_b_nig(2)"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado10 = false;
		}
		if (otro.name == "HW_doorR_f_nig(2)"){
			aux.text = "Pulsa E para Abrir la Puerta ";
			estado11 = false;
		}

		if (otro.name == "Ent_win_doorLR_add"){
			Escenas.text = "Pulsa E para Volver En la Tarde. ";
			estado12 = false;
		}

		if (otro.name == "Ent_wallL_doorLR_eve 1"){
			Escenas.text = "Pulsa E para Volver En la Noche. ";
			estado13 = false;
		}
		if (otro.name == "Ent_wallL_doorLR_nig"){
			Escenas.text = "Pulsa E para terminar el juego";
			estado14 = false;
		}

	}
	private void OnTriggerExit(Collider otro)
	{
		//Estos if son para controlar las puertas de por la mañana.//
		if (otro.name == "crm_doorR_b"){
			aux.text = "";
			if (puertaAbierta == true) {
				GameObject x = GameObject.Find ("crm_doorR_b");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("cerrarPuerta");
				puertaAbierta = false;
			}
			estado = true;
		}


		if (otro.name == "crm_doorL_f"){
			aux.text = "";
			if (puertaAbierta1 == true) {
				GameObject x = GameObject.Find ("crm_doorL_f");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta2Cerrada");
				puertaAbierta1 = false;
			}
			estado1 = true;
		}

		if (otro.name == "crm_doorL_b2"){
			aux.text = "";
			if (puertaAbierta2 == true) {
				//Debug.Log("Esta entrando aqui.");
				GameObject x = GameObject.Find ("crm_doorL_b2");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta3Cerrada");
				puertaAbierta2 = false;
			}
			estado2 = true;
		}

		if (otro.name == "crm_doorL_f2"){
			aux.text = "";
			if (puertaAbierta3 == true) {
				GameObject x = GameObject.Find ("crm_doorL_f2");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta4Cerrada");
				puertaAbierta3 = false;
			}
			estado3 = true;
		}

		//Estos if son para controlar las puertas de por la tarde.//
		if (otro.name == "HW_doorR_f_eve"){
			aux.text = "";
			if (puertaAbierta4 == true) {
				//Debug.Log("Esta entrando aqui.");
				GameObject x = GameObject.Find ("HW_doorR_f_eve");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta1EveningCerrada");
				puertaAbierta4 = false;
			}
			estado4 = true;
		}

		if (otro.name == "HW_doorR_b_eve"){
			aux.text = "";
			if (puertaAbierta5 == true) {
				GameObject x = GameObject.Find ("HW_doorR_b_eve");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta2EveningCerrada");
				puertaAbierta5 = false;
			}
			estado5 = true;
		}

		if (otro.name == "HW_doorL_b_eve"){
			aux.text = "";
			if (puertaAbierta6 == true) {
				GameObject x = GameObject.Find ("HW_doorL_b_eve");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta3EveningCerrada");
				puertaAbierta6 = false;
			}
			estado6 = true;
		}

		if (otro.name == "crm_doorL_f_eve"){
			aux.text = "";
			if (puertaAbierta7 == true) {
				GameObject x = GameObject.Find ("crm_doorL_f_eve");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta4EveningCerrada");
				puertaAbierta7 = false;
			}
			estado7 = true;
		}

		//Estos if son para controlar las puertas de por la noche://
		/*if (otro.name == "HW_doorR_f_nig"){
			aux.text = "";
			if (puertaAbierta8 == true) {
				GameObject x = GameObject.Find ("HW_doorR_f_nig");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta1NightAbierta");
				puertaAbierta8 = false;
			}
			estado8 = true;
		}*/

		if (otro.name == "HW_doorL_b_nig"){
			aux.text = "";
			if (puertaAbierta9 == true) {
				GameObject x = GameObject.Find ("HW_doorL_b_nig");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta2NightCerrada");
				puertaAbierta9 = false;
			}
			estado9 = true;
		}

		if (otro.name == "HW_doorL_b_nig(2)"){
			aux.text = "";
			if (puertaAbierta10 == true) {
				GameObject x = GameObject.Find ("HW_doorL_b_nig(2)");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta3NightCerrada");
				puertaAbierta10 = false;
			}
			estado10 = true;
		}

		if (otro.name == "HW_doorR_f_nig(2)"){
			aux.text = "";
			if (puertaAbierta11 == true) {
				GameObject x = GameObject.Find ("HW_doorR_f_nig(2)");
				Animator anima = x.GetComponent<Animator> ();
				anima.Play ("Puerta4NightCerrada");
				puertaAbierta10 = false;
			}
			estado11 = true;
		}

		if (otro.name == "Ent_win_doorLR_add"){
			Escenas.text = "";
			estado12 = true;
		}

		if (otro.name == "Ent_wallL_doorLR_eve 1"){
			Escenas.text = "";
			estado13 = true;
		}
		if (otro.name == "Ent_wallL_doorLR_nig"){
			Escenas.text = "";
			estado14 = true;
		}

	}
}
